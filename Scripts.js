var imageUri = "";
var allImagesUri = [];
var signalTreshold = 60;
var curPictIndex = 0;
var resReady = "";
var numOfPhotosSetting = false;
var timeSet = false;
var numOfPhotos = 1;
var photoSeconds = 1.0;
var click = new Audio("CameraClick.mp3");

function startFunc()
{
    if (navigator.appVersion.indexOf("Android") != -1)
    {
        var stl1 = ".ButtonClass {width: 700px; height: 117px; font-size: 50px;}";
        var stl2 = "body{background-image: url(\"img/BackgroundMobile.png\");}";
        var stl3 = "#Form{margin-left:10%;width:80%;margin-right:10%}";
        var editCSS = document.createElement('style')
        editCSS.innerHTML = stl1+"\n"+stl2+"\n"+stl3;
        document.body.appendChild(editCSS);
        var p = document.getElementById("CameraContainer");                                                                                    
        p.style.margin = "auto";
        p.style.width = window.screen.availWidth;
        p.style.height = window.screen.availHeight;
    }
}

function TakeAPhotoOpener()
{
    document.getElementById("hiddenDiv").style.display="block";
    var kk = document.getElementById("HiddenDiv");
    kk.style.width = window.screen.availWidth;
    kk.style.height = window.screen.availHeight;
    var kk = document.getElementById("Photo");
    kk.style.width = window.screen.availWidth;
    kk.style.height = window.screen.availHeight;
}

function FileOpener() {
    document.getElementById("file-input").click();
    document.body.scroll=true;
}

function standardDeviation(context,w,h,mean)
{
    var sumr = 0;
    var sumg = 0;
    var sumb = 0;
    n = w*h;
    var pixels = context.getImageData(0, 0, w, h).data;
    for(var i=0;i<pixels.length;i+=4){
        var pix = [pixels[i],pixels[i+1],pixels[i+2]]

        if(pix[0] == 0 && pix[1] == 0 && pix[2] == 0){
            n-=1;
            continue;
        }

        sumr+=(pix[0]-mean[0])**2;
        sumg+=(pix[1]-mean[1])**2;
        sumb+=(pix[2]-mean[2])**2;
    }

    return [Math.sqrt(sumr/n),Math.sqrt(sumg/n),Math.sqrt(sumb/n)];
}

function meanValue(context,w,h)
{
    var sumr = 0;
    var sumg = 0;
    var sumb = 0;
    n = w*h;
    var pixels = context.getImageData(0, 0, w, h).data;
    for(var i=0;i<pixels.length;i+=4){
        var pix = [pixels[i],pixels[i+1],pixels[i+2]]
        if(pix[0] == 0 && pix[1] == 0 && pix[2] == 0){
            n-=1;
            continue;
        }
        sumr+=pix[0];
        sumg+=pix[1];
        sumb+=pix[2];
    }

    return [sumr/n,sumg/n,sumb/n];
}

function RemoveNoise()
{
    if(allImagesUri.length==0){
        alert("You have to take a picture or load the image first!");
        return;
    }
    alert("Algorithm will start now. Wait for end message in popup.");
    for(var i = 0;i<allImagesUri.length;i++)
    {
        RemoveNoise1(i);
    }
    alert("Noise removal done!");
    resReady = "";
}

function RemoveNoise1(cur)
{
    var im = allImagesUri[cur];
    var canvas = document.createElement('canvas');
    canvas.width = im.width;
    canvas.height = im.height;
    var context = canvas.getContext('2d');
    context.drawImage(im, 0, 0);
    var mean = meanValue(context,im.width,im.height);
    var stDev = standardDeviation(context,im.width,im.height,mean);

    //Piksele sada dohvacamo s context.getImageData(x, y, 1, 1).data
    var dta = context.getImageData(0, 0, im.width, im.height);
    var pixels = dta.data;
    var comparer = [mean[0]+2*stDev[0],mean[1]+2*stDev[1],mean[2]+2*stDev[2]];

    //if(imagesTypes[cur].includes("png"))
        //comparer = [stDev[0],stDev[1],stDev[2]];
    
    for(var i=0;i<pixels.length;i+=4){
        var pix = [pixels[i],pixels[i+1],pixels[i+2]]
        if(pix[0] == 0 && pix[1] == 0 && pix[2] == 0)
            continue;
        if(pix[0]<comparer[0]){
            dta.data[i] = 0;
        }
        if(pix[1]<comparer[1]){
            dta.data[i+1] = 0;
        }
        if(pix[2]<comparer[2]){
            dta.data[i+2] = 0;
        }
    }

    context.putImageData(dta, 0, 0);
    document.getElementById("CameraImage").innerHTML = "<img id='Slik' src='"+canvas.toDataURL()+"'/>";
    delete allImagesUri[cur];
    var im = new Image();
    im.src = canvas.toDataURL();
    allImagesUri[cur] = im;
}

function CloseMe(){
    document.getElementById("hiddenDiv").style.display = "none";
}

async function doIt(data_uri,notLst)
{
    resReady = "";
    var im = new Image();
    imageUri = data_uri;
    im.src = data_uri;

    await im.decode();
    if(im.width>im.height)
        im.setAttribute("style", "transform: rotate(90deg)");

    var ratio = im.height/im.width;
    if(im.width>500){
        im.width = 500;
        im.height = ratio*500;
    }

    var canvas = document.createElement('canvas');
    canvas.width = im.width;
    canvas.height = im.height;
    canvas.getContext('2d').drawImage(im, 0, 0);

    data_uri = canvas.toDataURL();
    imageUri = canvas.toDataURL();
    delete im;
    im = new Image();
    im.src = canvas.toDataURL();

    allImagesUri.push(im);
    curPictIndex = allImagesUri.length-1;

    if(notLst)
        return;

    var kkk = document.getElementById("CameraContainer");

    kkk.style.width = canvas.width;
    kkk.style.height = canvas.height;

    if(allImagesUri.length>1){
        document.getElementById("LeftButt").style.display = "inline-block";
        document.getElementById("RightButt").style.display = "none";
    }

    document.getElementById("CameraContainer").style.display="inline-block";
    
    var kk = document.getElementById("CameraImage");
    kk.innerHTML = "<img id='Slik' src='"+data_uri+"'/>";
    kk = document.getElementById("Helper");
    kk.style.display="none";
    kk.innerHTML = data_uri;

    document.getElementById("curImg").innerHTML=curPictIndex+1+"/"+allImagesUri.length;
    delete im;
}

async function Calculate()
{
    if(resReady!=""){
        alert(resReady);
        return;
    }
    else if(allImagesUri.length == 0){
        alert("You have to take a picture or load the image first!");
        return;
    }

    alert("Calculation algorithm will start now. Sometimes it takes time so wait for end message in popup.");

    var im = new Image();
    im.src = imageUri;
    await im.decode();
    console.log(im.width);

    var canvas = document.createElement('canvas');
    canvas.width = im.width;
    canvas.height = im.height;
    var context = canvas.getContext('2d');
    context.drawImage(im, 0, 0);

    var dta = context.getImageData(0, 0, im.width, im.height);
    var pixels = dta.data;

    var max1 = 0;
    var max2 = 0;
    var max1_image = [];
    var max2_image = [];
    var overSignalTreshold = 0;
    var ovstToDoseMult = 1;
    var combinedDose = 0;
    var picture_doses = [];

    for(var i=0;i<pixels.length;i+=4){
        m1 = [];
        m2 = [];
        for(var c = 0; c<=2; c++){
            m1.push(0);
            m2.push(0);
        }
        max1_image.push(m1);
        max2_image.push(m2);
    }
    
    //Prvi prolaz kroz sve slike; Izracun doze po slici
    for(var l=0;l<allImagesUri.length;l++){
        delete im;
        delete dta;
        delete pixels;
        im = allImagesUri[l];
        canvas.width = im.width;
        canvas.height = im.height;
        context = canvas.getContext('2d');
        context.drawImage(im, 0, 0);
        dta = context.getImageData(0, 0, im.width, im.height);
        pixels = dta.data;
        overSignalTreshold = 0;

        for(var i=0;i<pixels.length;i+=4){
            for(var c = 0; c<=2; c++){
                var value = pixels[i+c];
                max1 = max1_image[i/4][c];
                max2 = max2_image[i/4][c];

                if(value == 0)
                    continue;
                
                if(value > max1){
                    max2 = max1;
                    max1 = value;
                    var signal = value - max2;
                    if(signal > signalTreshold)
                        overSignalTreshold++;
                }
                else if(value > max2 && value!=max1){
                    max2 = value;
                } 

                max1_image[i/4][c] = max1;
                max2_image[i/4][c] = max2;
            }
        }
        picture_doses.push(ovstToDoseMult*overSignalTreshold);
    }

    //Drugi prolaz kroz slike; Izracun ukupne doze za sve slike skupa
    overSignalTreshold = 0;
    
    im = allImagesUri[0];
    canvas.width = im.width;
    canvas.height = im.height;
    context = canvas.getContext('2d');
    context.drawImage(im, 0, 0);
    dta = context.getImageData(0, 0, im.width, im.height);
    pixels = dta.data;
    var maxSignal = 0;

    for(var i=0;i<pixels.length;i+=4){
        for(var c = 0; c<=2; c++){
            if(max1_image[i/4][c] == 0)
                continue;
            var signal = max1_image[i/4][c]-max2_image[i/4][c];
            maxSignal = Math.max(signal,maxSignal);
            if(signal>signalTreshold)
                overSignalTreshold++;
        }
    }
    combinedDose = ovstToDoseMult*overSignalTreshold/allImagesUri.length;
    resReady = "All pictures valuable pixels: "+picture_doses.toString()+"\n\nCombined valuable pixels: "+combinedDose+"\n\nMax signal detected: "+maxSignal;
    alert(resReady);
}

function delImg(){
    delete allImagesUri[curPictIndex];
    allImagesUri.splice(curPictIndex, 1);
    if(allImagesUri.length==0){
        document.getElementById("CameraContainer").style.display="none";
        imageUri = "";
    }

    else if(curPictIndex > 0){
        goLeft();
        if(curPictIndex==allImagesUri.length-1)
            document.getElementById("RightButt").style.display="none";
        else
            document.getElementById("RightButt").style.display="block";
    }
    else
        goRight();
        document.getElementById("LeftButt").style.display="none";
    
}

function goLeft(){
    curPictIndex--;
    document.getElementById("curImg").innerHTML=curPictIndex+1+"/"+allImagesUri.length;
    if(curPictIndex==0)
        document.getElementById("LeftButt").style.display="none";
    document.getElementById("RightButt").style.display="inline-block";
    document.getElementById("CameraImage").innerHTML = "<img id='Slik' src='"+allImagesUri[curPictIndex].src+"'/>";
}

function goRight(){
    curPictIndex++;
    document.getElementById("curImg").innerHTML=curPictIndex+1+"/"+allImagesUri.length;
    if(curPictIndex==allImagesUri.length-1)
        document.getElementById("RightButt").style.display="none";
    document.getElementById("LeftButt").style.display="inline-block";
    document.getElementById("CameraImage").innerHTML = "<img id='Slik' src='"+allImagesUri[curPictIndex].src+"'/>";
}

function SetNumOfPhotos(){
    numOfPhotosSetting = true;
    timeSet = false;
    var editCSS = document.createElement('style');
    editCSS.innerHTML = ".PictureSet{display:block;}\n.TimeSet{display:none;}\n.TresholdSet{display:none;}";
    document.getElementById("PictureAndTimeSet").style.display="flex";
    document.body.appendChild(editCSS);
}

function SetTime(){
    numOfPhotosSetting = false;
    timeSet = true;
    var editCSS = document.createElement('style');
    editCSS.innerHTML = ".PictureSet{display:none;}\n.TimeSet{display:block;}.TresholdSet{display:none;}";
    document.getElementById("PictureAndTimeSet").style.display="flex";
    document.body.appendChild(editCSS);
}

function SetTreshold(){
    numOfPhotosSetting = false;
    timeSet = false;
    var editCSS = document.createElement('style');
    editCSS.innerHTML = ".PictureSet{display:none;}\n.TimeSet{display:none;}.TresholdSet{display:block;}";
    document.getElementById("PictureAndTimeSet").style.display="flex";
    document.body.appendChild(editCSS);
    resReady = "";
}

function ConfirmThat(){
    if(numOfPhotosSetting){
        try{
            if(isNaN(document.getElementById("NumOfPicturesInput").value))
                throw "Not a number!";
            numOfPhotos = parseInt(document.getElementById("NumOfPicturesInput").value);
            document.getElementById("PictureAndTimeSet").style.display="none";
        }
        catch(error) {
            alert("Number of photos to be taken must be integer value! Invalid input!");
            document.getElementById("NumOfPicturesInput").value = 1;
        }
    }
    else if(timeSet){
        try{
            if(isNaN(document.getElementById("StopWatchInput").value))
                throw "Not a number!";
            photoSeconds = parseFloat(document.getElementById("StopWatchInput").value);
            document.getElementById("PictureAndTimeSet").style.display="none";
            webCamUpdate(photoSeconds);
        }
        catch(error) {
            alert("Number of photos to be taken must be integer value! Invalid input!");
            document.getElementById("StopWatchInput").value = 1;
        }
    }
    else{
        try{
            if(isNaN(document.getElementById("TreshInput").value))
                throw "Not a number!";
            signalTreshold = parseInt(document.getElementById("TreshInput").value);
            document.getElementById("PictureAndTimeSet").style.display="none";
        }
        catch(error) {
            alert("Number of photos to be taken must be integer value! Invalid input!");
            document.getElementById("StopWatchInput").value = 1;
        }
    }
}

function SaveRes()
{
    if(resReady!="")
        document.getElementById("folderSelector").click();
}

function SaveResult(path)
{

}


function OpenRes()
{

}
