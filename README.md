# Gamma shots

Open source project, made as my graduate work. It's an application intended for the analysis of photographs taken during gamma radiation, inspired by Joshua J. Cogliati, Kurt W. Derr and Jayson Wharton's work "Using CMOS Sensors in a Cellphone for Gamma Detection and Classification." Application is made only by using basic three front-end technologies: HTML+CSS+Javascript and all logic parts are written with pure javascript (no backend language used). 
App also uses exsisting open-source library: webcam.js (https://github.com/jhuckaby/webcamjs) to access webcam and is modified for the needs of my application. Modified code of webcam.js can be found here in "webcamjs-master" folder. 
App can be run from any device (PC, smartphone, tablet...) by using any web browser and in case of using a smartphone or tablet, the use of a built-in web browser is recommended.
You can both take photos through the application interface, or load them from the device memory. 
After successfull calculation, app gives you the information about approximate radiation dose of gamma source. 
It is reccomended to tape something over your camera while taking photos to prevent access of visible light to the camera.
To run app, just run "Index.html" and allow access to your webcam when app asks you.
